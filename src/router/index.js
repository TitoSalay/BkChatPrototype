import Vue from 'vue'
import Router from 'vue-router'
import Contact from '../components/Contact.vue'
import Main from '../components/Main.vue'
import CastMembers from '../components/castMembers.vue'


Vue.use(Router)

export default new Router({
  routes: [
    {path:'/', component:Main},
    {path:'/contact', component:Contact},
    {path:'/cast_members', component:CastMembers}],
  mode:'history'
})
